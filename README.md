# ubuntu-dev-android

# [ubuntu-x64-dev-android](https://hub.docker.com/r/forumi0721ubuntux64build/ubuntu-x64-dev-android/)
[![](https://images.microbadger.com/badges/version/forumi0721ubuntux64build/ubuntu-x64-dev-android.svg)](https://microbadger.com/images/forumi0721ubuntux64build/ubuntu-x64-dev-android "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721ubuntux64build/ubuntu-x64-dev-android.svg)](https://microbadger.com/images/forumi0721ubuntux64build/ubuntu-x64-dev-android "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721ubuntux64build/ubuntu-x64-dev-android.svg?style=flat-square)](https://hub.docker.com/r/forumi0721ubuntux64build/ubuntu-x64-dev-android/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721ubuntux64build/ubuntu-x64-dev-android.svg?style=flat-square)](https://hub.docker.com/r/forumi0721ubuntux64build/ubuntu-x64-dev-android/)



----------------------------------------
#### Description

* Distribution : [Ubuntu](https://www.ubuntu.com/)
* Architecture : x64
* Appplication : -
    - Android development environment
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -i -t --rm \
           -e RUN_USER_NAME=<user_name> \
           forumi0721ubuntux64/ubuntu-x64-dev-android:latest
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | login username (default:forumi0721)              |



----------------------------------------
* [forumi0721ubuntux64/ubuntu-x64-dev-android](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-dev-android/)

